<?php

/**
 * @file
 * Creates a field to store EPS files
 */


// Load all Field module hooks for Image.
module_load_include('inc', 'imagefield_eps', 'includes/field');

/**
 * Implements hook_file_move().
 */
function imagefield_eps_file_move($file, $source) {
  // Delete any image derivatives at the original image path.
  imagefield_eps_path_flush($source->uri);
}

/**
 * Implements hook_file_delete().
 */
function imagefield_eps_file_delete($file) {
  // Delete any image derivatives of this image.
  imagefield_eps_path_flush($file->uri);
}

/**
 * Clear cached versions of a specific file in all styles.
 *
 * @param string $uri
 *   The Drupal file path to the original image.
 */
function imagefield_eps_path_flush($uri) {
  $uri_target = file_uri_target($uri);

  $filename = str_replace(array('.eps', '.EPS'), '.png', $uri_target);
  $target = drupal_realpath(file_build_uri('imagefield_eps_images')) . '/' . $filename;
  if (file_exists($target)) {
    file_unmanaged_delete($target);
  }
}
